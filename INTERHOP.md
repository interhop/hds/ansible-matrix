# Development Prerequisite

Locally:

- docker
- python3 (`pip install pre-commit`)
- run pre-commit install


# Git workflow

- add upstream remote https://github.com/spantaleev/matrix-docker-ansible-deploy.git
- read the changelog before mergint upstream into interhop https://github.com/spantaleev/matrix-docker-ansible-deploy/blob/master/CHANGELOG.md
- merge upstream and push to remote

# Use ansible docker

```sh
# INSTALL ANSIBLE LOCALLY
docker build -t ansible:latest .

# INSTALL ON REMOTE
./install.matrix-test

# Decrypt with vault
./crypt encrypt matrix-test.interhop.org
./crypt decrypt matrix-test.interhop.org
```

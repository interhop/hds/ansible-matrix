FROM docker.io/devture/ansible:latest
WORKDIR /work
COPY Makefile .
COPY requirements.yml .
RUN make roles
RUN mkdir -p /root/.ansible
RUN mv /work/roles /root/.ansible/
